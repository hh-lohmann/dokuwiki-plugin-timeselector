base   timeselector
author hh.lohmann
email  hh.lohmann@yahoo.de
date   2014-05-26
name   timeselector plugin
desc   selecting time in form inputs - port of https://github.com/nicolaszhao/timeselector
url    https://bitbucket.org/hh-lohmann/dokuwiki-plugin-timeselector