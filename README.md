# timeselector Plugin for DokuWiki

Adding functionality to form inputs for easily selecting time data,
implementing a ported version of https://github.com/nicolaszhao/timeselector
(version 2014-02-17) with changed default settings for 24 hour display.

Special handling is done to use with the data plugin by a type alias
"_timeselector".


If you install this plugin manually, make sure it is installed in
lib/plugins/timeselector/ - if the folder is called different it
will not work!

Please refer to http://www.dokuwiki.org/plugins for additional info
on how to install plugins in DokuWiki.

----
Copyright (C) hh.lohmann <hh.lohmann@yahoo.de>
Copyright timeselector jQuery plugin (C) Nicolas Zhao

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See the COPYING file in your DokuWiki folder for details
